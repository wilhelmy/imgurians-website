<?
include_once("bootstrap.inc.php");
include_once("header.inc.php");
include_once("captcha.inc.php");

$captcha = new Captcha();

$error = "";
if ($_POST["nick"] && $_POST["quote"])
{
  if ($captcha->TestValues($error))
  {
    $a = array();
    $a["nick"] = $_POST["nick"];
    $a["comment"] = $_POST["comment"];
    $a["quote"] = $_POST["quote"];
    $a["submit_time"] = time();
    $a["out_of_context"] = ($_POST["context"] == "on") ? 1 : 0;
    $id = SQLLib::InsertRow("quotes",$a);
    
    header("Location: ".SITE_URL."quotes/#q".$id);
    exit();
  }
}
if ($error)
  printf("<div class='error'>%s</div>\n",_html($error));

?>
<script language="JavaScript" type="text/javascript">
<!--
// $Id: submit.html,v 1.7 2014-06-18 18:05:51 mw Exp $
var ts_reg = /^\[?[0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2})?\]? /mg;
var checked = false; // quote was not yet checked for timestamps

function strip_timestamps() {
	// Source: http://quotes.umlaut.hu/add.php
	var q = document.getElementById("quote");
	var t = q.value;
	var no_ts = t.replace(ts_reg, '');
	q.value = no_ts.replace(/^<[\s@+]?/mg, '<');
}

function xhrsubmit() {
	var q = $("#quote").val();

	// Check the quote for timestamps, refuse at first.
	if (!checked && ts_reg.test(q)) {
		alert("It looks like your quote still has timestamps in it.\n\n"+
		"Please hit the 'Strip timestamps' button or press 'Submit'"+
		" a second time if you are sure that the timestamps are important.");
		checked = true;
		return false;
	}

	$.ajax("submit.pl", {
		type: "POST",
		data: {
			nick:    $("#nick").val(),
			context:($("#context").prop('checked') ? 'on' : 'off'),
			quote:   $("#quote").val(),
			comment: $("#comment").val(),
			utf8:    $("#utf8").val()
		}
	}).done(function(response) {
		if (response.match(/success/)) { // HACK
			alert("Your quote was submitted");
			$("#quote").val("");
			$("#comment").val("");
			$("#context").prop("checked", false);
			checked = false;
		} else {
			alert("There was an error adding your quote.\n"
			+"Please make sure to include your nickname,\n"
			+"and don't forget your quote either!");
		}
	});
	return false;
}

document.observe("dom:loaded",function() {
	//$("#theform").submit(xhrsubmit);
	$("quote-submit").insert(new Element("input",{
		type:    "button",
		value:   "Strip timestamps",
		id:      "strip",
		onclick: "strip_timestamps(); return false;"
	}));
});
//-->
</script>
<h2>Rules</h2>
<ul>
<li>
Please format your quotes the following way (i.e. nicks enclosed in &lt;&gt;, no timestamps):
<div class="quote example">
<pre>&lt;nick&gt; hello!
&lt;ente&gt; Hi!
* nick poops</pre></div>
</li>
<li>No editing or altering quotes in ways not intended by the person who said them (taking them out of context is okay, but please check the "Quote was taken grossly out of context" checkbox if you do)</li>
<li>No concatenating of multiple quotes unless they clearly belong to each other. Submit each unrelated quote individually.</li>
<li>Please leave irrelevant conversation pieces out.</li>
<li>No spamming. Don't be an ass. We need to moderate this stuff and you really don't want to anger me. If you do, chances are we take this whole thing offline again.</li>
<li>Submit <b>quotes</b> please, do not make up stuff that was never said. From this point on, quotes which were never said will be deleted.</li>
<li>Duplicates will be EXTERMINATED.</li>
</ul>
<h2>Submit a quote</h2>
<form id="quote-submit" method="post">
  <input type="hidden" name="utf8" value="&#2603;" id="utf8">
  <label for="nick">Your nick:</label>
  <input name="nick" type="text" id="nick" maxlength="15" value="<?=_html($_POST["nick"]?:($currentUser?$currentUser->name:""))?>">
  <label for="nick">Comment (optional, max 140 chars):</label>
  <input name="comment" type="text" id="comment" maxlength="140" value="<?=_html($_POST["comment"])?>">
  <input type="checkbox" id="context" name="context">
  <label for="context">Quote was taken grossly out of context</label>
  <label for="quote">Quote:</label>
  <textarea id="quote" name="quote"><?=_html($_POST["quote"])?></textarea>
  
  <? $captcha->PrintFormComponents(); ?>
  <input id="submit" type="submit" value="Submit">
</form>
<?

include_once("footer.inc.php");
?>