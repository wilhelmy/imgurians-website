CREATE TABLE "news" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "userID" integer NOT NULL,
  "date" text NOT NULL,
  "title" text NOT NULL,
  "contents" text NOT NULL,
  FOREIGN KEY ("userID") REFERENCES "users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE TABLE "users" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "name" text NOT NULL,
  "imgurID" integer NOT NULL,
  "countryCode" text NOT NULL DEFAULT '',
  "email" text NOT NULL DEFAULT '',
  "website" text NOT NULL DEFAULT '',
  "facebook" text NOT NULL DEFAULT '',
  "twitter" text NOT NULL DEFAULT '',
  "instagram" text NOT NULL DEFAULT '',
  "snapchat" text NOT NULL DEFAULT '',
  "imgur" text NOT NULL DEFAULT '',
  "voucherUserID" integer NULL,
  "voucherCode" text NOT NULL
);