<?
///////////////////////////////////////////////////////////////////////////////

function _html( $s )
{
  return htmlspecialchars($s,ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5,"utf-8");
}
function _js( $s )
{
  return addcslashes( $s, "\x00..\x1f'\"\\/" );
}
function _like( $s )
{
  return addcslashes($s,"%_");
}

function shortify( $text, $length = 100 )
{
  if (mb_strlen($text,"utf-8") <= $length) return $text;
  $z = mb_stripos($text," ",$length,"utf-8");
  return mb_substr($text,0,$z?$z:$length,"utf-8")."...";
}

function is_logged_in()
{
  return !!$_SESSION["userID"];
}
function generate_voucher()
{
  return strtoupper(substr(sha1("K7bUAJ3W7p6g6Dz".time().rand(1000,9999)),0,8));
}
?>