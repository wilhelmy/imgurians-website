<?
include_once("bootstrap.inc.php");

header("Content-type: application/rss+xml; charset=utf-8");

echo "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";

$news = SQLLib::SelectRows("select * from quotes order by id desc limit 20");
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>#imgurians quotes</title>
		<link><?=SITE_URL?>quotes</link>
		<atom:link href="<?=SITE_URL?>rss/quotes" rel="self" type="application/rss+xml" />
		<description>Quotes from #imgurians on IRCnet</description>
<? foreach ($news as $post) { ?>
		<item>
			<title>#<?=$post->id?> - <?=_html(shortify($post->quote,80))?></title>
			<link><?=SITE_URL?>quotes/#q<?=$post->id?></link>
			<guid isPermaLink="false">imguriansquotes<?=$post->id?></guid>
			<pubDate><?=date("r",strtotime($post->submit_date))?></pubDate>
			<description><![CDATA[<?=_html(shortify($post->quote,500))?>]]></description>
		</item>
<? } ?>
  </channel>
</rss>