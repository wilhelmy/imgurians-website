<?
include_once("bootstrap.inc.php");
include_once("header.inc.php");

/////////////////////////////////////////////////

if ($imgur && is_logged_in() && $_POST["albumURL"])
{
  $code = $_POST["albumURL"];
  if (preg_match("/imgur.com\/a\/([a-zA-Z0-9]+)/",$code,$matches))
    $code = $matches[1];
  
  $a = array();
  $a["userID"] = $currentUser->id;
  $a["contentsJSON"] = json_encode( $imgur->GetAlbum( $code ) );
  SQLLib::InsertRow("galleries",$a);
}

function galleryHack($imageCodes)
{
  echo "<div class='images'>";
  foreach($imageCodes as $code)
    echo "<a href='http://imgur.com/gallery/".$code."'><img src='http://i.imgur.com/".$code."b.png' title='source: imgur.com' /></a>";
  echo "</div>";
}

if (!$_GET["show"])
{
  echo "<h2>Look at the thing</h2>";

  echo "<div class='gallery'>";
  echo "<h3>Channel announcements on imgur</h3>";
  galleryHack(array(
    "KXwW7",
    "lWSSR",
    "fNkr9",
    "u0G7CWY",
    "VPJzr76",
    "Tn0xZYC",
  ));
  echo "</div>";
}

/////////////////////////////////////////////////

echo "<h2>Our face is like a song</h2>";

function showGallery($data, $limit = -1, $id = null)
{
  $images = $data["data"]["images"];
  if ($limit > 0)
  {
    shuffle($images);
    $images = array_slice( $images, 0, $limit );
  }
  echo "<div class='gallery'>";
  echo "<h3>"._html($data["data"]["title"])."</h3>";
  echo "<div class='submitter'>";
  if ($data["data"]["account_url"])
    echo "by "._html($data["data"]["account_url"]);
  echo " on ".date("Y-m-d H:i:s",$data["data"]["datetime"]);
  echo "</div>";
  
  echo "<div class='images'>";
  foreach($images as $image)
  {
    echo "<a href='http://imgur.com/gallery/".$image["id"]."'><img src='http://i.imgur.com/".$image["id"]."b.png' title='"._html($image["title"])."' /></a>";
  }
  echo "</div>";
  if ($limit != -1 && $id !== null)
    echo "<p><a href='".SITE_URL."gallery/?show=".$id."'>See more &raquo;</a></p>";
    
  echo "</div>";
  
}

$gallery = SQLLib::SelectRow(sprintf_esc("select * from galleries where id = %d",$_GET["show"]));
if ($gallery)
{
  $data = json_decode( $gallery->contentsJSON, true );
  showGallery( $data );
}
else
{
  $galleries = SQLLib::SelectRows(sprintf_esc("select * from galleries order by id"));
  foreach($galleries as $gallery)
  {
    $data = json_decode( $gallery->contentsJSON, true );
    showGallery( $data, 4, $gallery->id );
  }
}
if (is_logged_in())
{
  echo "<h2>Add new album</h2>";
  
  echo "<form method='post'>";
  echo "  <label for='name'>Album imgur URL</label>";
  echo "  <input type='url' name='albumURL' id='albumURL' required='yes'/>";
  echo "  <input type='submit' value='Send!'>";
  echo "</form>";
}

include_once("footer.inc.php");
?>
