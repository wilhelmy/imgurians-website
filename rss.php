<?
include_once("bootstrap.inc.php");

header("Content-type: application/rss+xml; charset=utf-8");

echo "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";

$news = SQLLib::SelectRows("select news.*, users.name from news left join users on users.id = news.userid order by date desc, id desc ".($_GET["all"]?"":" limit 5"));
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>#imgurians</title>
		<link><?=SITE_URL?></link>
		<atom:link href="<?=SITE_URL?>rss" rel="self" type="application/rss+xml" />
		<description>May contain nuts!</description>
<? foreach ($news as $post) { ?>
		<item>
			<title><?=_html($post->title)?></title>
			<link><?=SITE_URL?>#news<?=$post->id?></link>
			<guid isPermaLink="false">imgurians<?=$post->id?></guid>
			<pubDate><?=date("r",strtotime($post->date))?></pubDate>
			<description><![CDATA[<?=_html(shortify($post->contents,500))?>]]></description>
		</item>
<? } ?>
  </channel>
</rss>