<?
include_once("bootstrap.inc.php");
include_once("header.inc.php");

echo "<h2>Newsy things!</h2>";

$total = SQLLib::SelectRow("select count(*) as c from news")->c;
$news = SQLLib::SelectRows("select news.*, users.name from news left join users on users.id = news.userid order by date desc, id desc ".($_GET["all"]?"":" limit 5"));
foreach($news as $v)
{
?>
    <article id='news<?=$v->id?>'>
      <h3><time><?=_html(date("Y-m-d",strtotime($v->date)))?></time> - <?=_html($v->title)?></h3>
      <div class='submitter'>by <?=$v->name?></div>
      <div class='contents'>
      <p><?
      $s = _html($v->contents);
      $s = str_replace("\r\n","\n",$s);
      $s = preg_replace("/\n{2,}/","</p>\n<p>",$s);
      $s = preg_replace("/([a-z]+:\/\/\S+)/","<a href='$1'>$1</a>",$s);
      echo $s;
      ?></p>
      </div>
    </article>
<?
}
printf("<div id='news_nav'>");
if ($total > 5 && !$_GET["all"])
  printf("<a href='%snews/archive'>Older news &raquo;</a> ",SITE_URL);
if (is_logged_in())
  printf("<a href='%snews/submit'>Submit news &raquo;</a> ",SITE_URL);
printf("</div>");

include_once("footer.inc.php");
?>